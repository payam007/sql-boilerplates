from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey

# sync_engine = create_engine("postgresql+psycopg://payam:chelen007%@127.0.0.1:5432/db")
sync_engine = create_engine("sqlite:///sqlite3.db")

metadata_obj = MetaData()


def create_tables():
    Table(
        "test_user",
        metadata_obj,
        Column("id", Integer, primary_key=True),
        Column("name", String(30)),
    )
    Table(
        "test_table",
        metadata_obj,
        Column("id", Integer, primary_key=True),
        Column("location", String(30)),
        Column("desc", String),
        Column("user_id", ForeignKey("test_user.id"), nullable=False),
    )
    metadata_obj.create_all(sync_engine)


def load_table():
    jobs_table = Table("jobs", metadata_obj, autoload_with=sync_engine)
    return jobs_table
