from datetime import datetime
from Bio import SeqIO


def parse_fasta(file_path):
    with open(file_path, 'r') as file:
        sequences = {}
        current_header = None
        for line in file:
            line = line.strip()
            if line.startswith('>'):
                current_header = line[1:]  # Remove '>'
                sequences[current_header] = []
            else:
                if current_header is not None:
                    sequences[current_header].append(line)

        # Join sequence lines
        for hdr in sequences:
            sequences[hdr] = ''.join(sequences[hdr])

    # Print parsed sequences
    # for header, sequence in sequences.items():
    #     print(f">{header}\n{sequence}\n")


def parse_fasta_generator(file_path):
    def fasta_generator(path):
        with open(path, 'r') as file:
            inner_header = None
            inner_sequence = []
            for line in file:
                line = line.strip()
                if line.startswith('>'):
                    if inner_header:
                        yield inner_header, ''.join(inner_sequence)
                    inner_header = line[1:]  # Remove '>'
                    inner_sequence = []
                else:
                    inner_sequence.append(line)
            if inner_header:
                yield inner_header, ''.join(inner_sequence)

    parsed_sequences = {}
    for header, sequence in fasta_generator(file_path):
        parsed_sequences[header] = sequence

    print("Done")

    # Print parsed sequences
    # for header, sequence in parsed_sequences.items():
    #     print(f">{header}\n{sequence[:50]}...\n")  # Print first 50 bases for brevity


def parse_fasta_biopython(file_path):
    python_dict = {}
    for record in SeqIO.parse(file_path, "fasta"):
        python_dict[record.id] = record.seq
        # print(f"ID: {record.id}, Length: {len(record.seq)}")
    print("Dine")


def parse_fasta_stream(file_path):
    python_dict = {}
    with open(file_path, "r") as handle:
        fasta_iterator = SeqIO.parse(handle, "fasta")
        for record in fasta_iterator:
            # Process each record on the fly
            python_dict[record.id] = record.seq
            # print(f"ID: {record.id}, Length: {len(record.seq)}")
    print("Done")


def parse_with_bio_index(path):
    index = SeqIO.index(path, "fasta")
    for id, seq in index.items():
        print(id, seq)
    print("Done")
    return index


if __name__ == "__main__":
    start_time = datetime.now()
    parse_with_bio_index('./pdb_seqres.txt')
    end_time = datetime.now()
    print(end_time - start_time)
