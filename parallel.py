from Bio import SeqIO
import pandas as pd
import dask.dataframe as dd
from dask.distributed import Client
import os


def parse_fasta(file_path):
    for record in SeqIO.parse(file_path, "fasta"):
        yield {"id": record.id, "sequence": str(record.seq)}


def save_chunks_to_disk(file_path, output_dir, chunk_size=10000):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    chunk = []
    chunk_idx = 0
    for i, record in enumerate(parse_fasta(file_path)):
        chunk.append(record)
        if (i + 1) % chunk_size == 0:
            df = pd.DataFrame(chunk)
            df.to_parquet(f"{output_dir}/chunk_{chunk_idx}.parquet")
            chunk = []
            chunk_idx += 1

    if chunk:
        df = pd.DataFrame(chunk)
        df.to_parquet(f"{output_dir}/chunk_{chunk_idx}.parquet")


if __name__ == '__main__':
    # Specify the path to your FASTA file and the output directory
    fasta_file_path = "./pdb_seqres.txt"
    output_directory = "./parquets"

    # Save chunks to disk
    save_chunks_to_disk(fasta_file_path, output_directory)

    dask_df = dd.read_parquet(f"{output_directory}/*.parquet")

    client = Client()  # Adjust these numbers based on your machine's capabilities
    print(client)

    print(dask_df.iloc[:, 1].compute())

    # To perform operations and compute results
    #
    print(dask_df.head())
