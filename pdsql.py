import pandas as pd
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey


def get_db_engine():
    return create_engine('sqlite:///df_sqlite', echo=False)


def init__tables():
    engine = get_db_engine()
    metadata_obj = MetaData()
    Table(
        "students",
        metadata_obj,
        Column("id", Integer, primary_key=True),
        Column("name", String(30)),
    )
    Table(
        "history",
        metadata_obj,
        Column("id", Integer, primary_key=True),
        Column("student_id", ForeignKey("students.id"), nullable=False),
        Column("record_id", Integer),
        Column("score", Integer),
    )
    metadata_obj.create_all(engine)


def init__dfs():
    engine = get_db_engine()
    students = pd.DataFrame({
        'id': [2, 3, 4],
        'name': ['Alice', 'Bob', 'Charlie']
    })
    # students = students.drop(columns=["index"])
    history = pd.DataFrame({
        'record_id': [1, 2, 3],
        'student_name': ['Alice', 'Bob', 'Bob'],  # This references the 'name' in students
        'score': [85, 90, 95]
    })
    hist = history.merge(students, left_on='student_name', right_on='name', how='left')
    hist = hist.rename(columns={'id': 'student_id'})
    hist = hist.drop(columns=['student_name', 'name'])

    students.to_sql(name='students', con=engine, if_exists='append', index=False)
    hist.to_sql(name='history', con=engine, if_exists='append', index=False)


if __name__ == '__main__':
    # init__tables()
    init__dfs()
