import pandas as pd
from sqlalchemy import text

from db import sync_engine, create_tables, load_table


def raw_query():
    with sync_engine.connect() as conn:
        result = conn.execute(text("SELECT COUNT(*) FROM test_user"))
        print(result.all())


def sample_detailed_sql():
    with sync_engine.connect() as conn:
        results = conn.execute(text("SELECT ref_id, title FROM jobs"))
        for row in results:
            print(f"ref_id : {row.ref_id}, title : {row.title} ")


def send_params_sql():
    with sync_engine.connect() as conn:
        results = conn.execute(text("SELECT ref_id, title FROM jobs where ref_id = :num "), {"num": 3926358355})
        for row in results:
            print(f"ref_id : {row.ref_id}, title : {row.title} ")


def send_multiple_params():
    with sync_engine.connect() as conn:
        conn.execute(
            text("INSERT INTO jobs (ref_id, title, link) VALUES (:ref_id, :title, :link)"),
            [{"ref_id": 11, "title": "Ahmad", "link": "google.com"},
             {"ref_id": 13, "title": "Ali", "link": "google.com"}],
        )
        conn.commit()


def load_tsv_file():
    df = pd.read_csv('./output-onlinetsvtools.txt', sep='\t', header=None)
    data = df.to_dict(orient='records')
    return data


if __name__ == "__main__":
    # raw_query()
    # load_tsv_file()
    # sample_detailed_sql()
    # send_params_sql()
    # send_multiple_params()
    # create_tables()
    raw_query()
